import Foundation

// TODO: You may add/edit/remove any default code here

var inputAction: String = ""
var ArrayofMenu : [Menu] = [Menu(menuID: "F01", menuName: "Nasi Padang"), Menu(menuID: "F02", menuName: "Chicken Satay"), Menu (menuID: "F03", menuName: "Gado-Gado"), Menu (menuID: "B01", menuName: "Ice Tea"), Menu (menuID: "B02", menuName: "Mineral Water")]
var inputMenu : String = ""
var arrayofShoppingCart : [ShoppingCart] = []

func showMainMenu() {
    print("""

    =================================
       Academy's Cafe & Resto v2.0
    =================================

    Options:
    [1] Buy Food
    [2] Shopping Cart
    [x] Exit

    """)
    
    print("Your choice? ", terminator: "")
}

func showShoppingCart () {
    print ("""
        Shopping Cart (\(arrayofShoppingCart.count) items):
        """)
    for item in arrayofShoppingCart {
        print ("\(item.foodquantity) \(item.menuName)")
    }
     print ()
}
while inputAction.lowercased() != "x" {
    showMainMenu()
    inputAction = readLine() ?? ""
    
    if inputAction == "1" {
        print ("""
            Hi, we have 5 Food and Beverage option for you!
            =============================================
            
            [\(ArrayofMenu[0].menuID)] \(ArrayofMenu[0].menuName)
            [\(ArrayofMenu[1].menuID)] \(ArrayofMenu[1].menuName)
            [\(ArrayofMenu[2].menuID)] \(ArrayofMenu[2].menuName)
            [\(ArrayofMenu[3].menuID)] \(ArrayofMenu[3].menuName)
            [\(ArrayofMenu[4].menuID)] \(ArrayofMenu[4].menuName)
            [Q] Back to Main Menu
            """)
        
        print("Your choice? ", terminator: "")
        
    }
    
    print()
    
    while inputMenu.lowercased () != "q" {
        print ("Your F&B choice?", terminator: "")
        print ()
        inputMenu = readLine() ?? ""
        
        if inputMenu !=  "q" {
            var menuName : String = ""
            for menu in ArrayofMenu{
                if menu.menuID == inputMenu.uppercased(){
                    menuName = menu.menuName
                    break
                    
                }
            }
            
            let shoppedItem = ShoppingCart (menuID: inputMenu.uppercased(), menuName: menuName)
            
            print ("How many\(menuName) you want to buy? ", terminator :"")
            
            let amountofFood = readLine() ?? "1"
            
            shoppedItem.foodquantity = Int (amountofFood) ?? 0
            
            arrayofShoppingCart.append(shoppedItem)
            
            showShoppingCart()
            
           
        }
        else if inputAction == "2" {
            if arrayofShoppingCart.count == 0 {
                print ("Your  shopping list is empty, please buy something")
            }
            else {
                showShoppingCart()
            }
        }
    }
    
}


